apt-get install python, python-pip, virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
./reset.sh
python manage.py runserver
